/*	main.h (universal header) Ver 0.72

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, version 3 (or any later).  

This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without 
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
General Public License for more details.  You should have received a copy of the GNU General 
Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.		*/

#include <errno.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>
#include <gtk/gtk.h>
#include <fcntl.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <ctype.h>
#include <pthread.h>
#include <limits.h>

	/* max full pathname */
#ifdef PATH_MAX
	#define MPTH PATH_MAX 
#else
	#define MPTH 4096
#endif
#define FLESZ MPTH+1024	/* filelist entry length */
#define NAMELEN 128 	/* length of seefile "name" */
#define MAXTERMLEN 1024	/* max length of term from command line */
#define TRANS_MAX 8192 /* buffer size for socket operations (should hold MPTH+MAXTERMLEN+more) */
#define SECLEN 16	/* max length manpage section id */
#define SEE_VERSION "0.72"
#define PAD 4		/* frame padding */

typedef struct {
	GtkWidget *wgt;
	GtkWidget *image1;
	GtkWidget *image2;
	int sw;
} forblink; 

typedef struct {
	char *txtfnt;	/* the text font */
	int width;	/* text window dimensions */
	int height;
	char wrap;	/* wrap mode for text view */
	const gchar *tbcolor;	/* text window background color */
	char bConfirm;	/* use popup dialog confirm */
	char bRedirect;	/* redirect stderr for ctrl-x "execute" operations */
	int watchtime;	/* duration for file watches */
	char *filelist;	/* filelist path */
	char sock[108]; /* seesocket name */
	char *editor;	/* editor command */
	char *copyto;	/* directory for ctrl-o "copy out" commands */
	char *seedata;	/* file to store bookmarks & highlights in */
	int tailat;		/* boundary for tailing large files */
} forconfig;

typedef struct {
	GtkWidget *hbx;
	GtkWidget *back;
	GtkWidget *current;
	GtkWidget *forward;
	GtkTreeSelection *select;
} navbar;

typedef struct {
	char path[MPTH+1];
	char name[NAMELEN+1];
	char type;	/* 'R'egular or 'M'anpage */
	char sec[SECLEN+1];
} seefile;

typedef struct {
	pthread_cond_t cond;	/* currently unused */
	pthread_mutex_t lock;
	int data;
} threadlock;

struct matchspec {
	int bgn;
	int end;
} rgxp;

/* see.c */
void addtohistory (char *line);
void apropos ();
void applytag (int Ts, int Te, char *tag);
gboolean blinktoggle (forblink *ff);
void bookmark_list();
void changewrapmode (GtkWidget *RButton, char mode);
void clear(int opt);
void closeFL_window (GtkWidget *ignored, gpointer *sbwin);
int confirm_popup (char *title, char *messg);
void copytoX (GtkWidget *ignored, int opt); 
gint cursorline();
void cursor_tofound (GtkWidget *widget, int way);
gboolean delbmk (GtkWidget *treeview, GdkEventButton *MoBt, GtkListStore *liststore);
void destroy_widget (GtkWidget *widget, GtkWidget *dead);
gboolean DnDdrop (GtkWidget *widget, GdkDragContext *context, gint x, gint y, guint time, gpointer *NA); 
void DnDleave (GtkWidget *widget, GdkDragContext *context, guint time, gpointer *NA);
gboolean DnDmotion (GtkWidget *widget, GdkDragContext *context, gint x, gint y, GtkSelectionData *seld, guint ttype, guint time, gpointer *NA);
void DnDreceive (GtkWidget *widget, GdkDragContext *context, gint x, gint y, GtkSelectionData *seld, guint ttype, guint time, gpointer *data);
void dobmks (); 
void error_popup (char *messg); 
void exec_proc (); 
void exeFSlisting (char *listing);
gboolean FentKpress (GtkWidget *widget, GdkEventKey *kyprs, gpointer *data);
void file_out (void); 
void fileselect (GtkTreeView *treeview, GtkTreePath *treepath, GtkTreeViewColumn *treecol, GtkWidget *swin); 
int getagcoords (GtkTextTag *tag, char *tagline);
void gotobmk ();
void gotoCursor (GtkWidget *ignored);
void grabfocus (GtkWidget *ignored);
void handlequit();
void help (void); 
void highlight (GtkWidget *widget, gchar *tag);
seefile *initSF (char *path, char type, char *sec);
void list2view (GtkListStore* ls);
int loadfile (int setline);
gboolean loadlarge (int len); 
int loadlist (char list[27][FLESZ]); 
int loadman (int setline);
int loadnew (char *file, char *sec, int setline);
void mainmenu();
gboolean mousevent (GtkWidget *widget, GdkEventButton *MoBt, gpointer *data);
int nextfind (char *haystack, char *needle); 
gboolean nomax (GtkWidget *widget, GdkEventWindowState *ptr, gpointer *data); 
void numberlines ();
void placebmk ();
forconfig *reconfigure(GtkWidget *ignored);
void removetags (GtkWidget *widget, gpointer *data); 
void rescanFileList ();
gboolean reload();
void savebmks (GtkWidget *widget, GtkWidget *subwin); 
void scrollToIter (GtkTextIter *iter);
void scrolltoline (GtkWidget *widget, gint line);
void searchlight ();
void setFL_selected ();
void setNULL (GtkWidget *ignored, gpointer *ptr);
void setwrapmode ();
seefile *SFcopy (seefile *orig);
void shiftFilelist (GtkWidget *ignored, int val);
void showconfig(forconfig *cfg);
void showlist();
void sortbmarks();
gboolean tailfile ();
gboolean testuni (gunichar CHR, short int chr);
gchar *textline (int line);
void toedit ();
gboolean TxTKpress (GtkWidget *widget, GdkEventKey *kyprs, gpointer *data);
int update_filelist (seefile *old, int lastline); 
void usage (char *name);
void usebmk (GtkTreeView *treeview, GtkTreePath *treepath, GtkTreeViewColumn *treecol, GtkWidget *swin);
void watchfile ();

/* util.c */
int buflen (char *buffer);
int copytmp (char *tmp, char *perm);
char *defluff (char *string);
void *ec_malloc (size_t bytes);
int fdreadin (char *source, char *buffer, char end, int max, int offset); 
int filelen (char *file); 
char *linein (FILE *stream);
void onenwline (char *buffer);
int regexp (char *string, char *rgxp);
char *returnline (char *file, char *match);
void sortintray (int *ray, int len); 
int streamline (FILE *IN, char **line, int bsize); 
int strexd (int EXPlen, char *string);
char *strsub (const char *string, char *newname);
int utfadj (char *buffer, int pos); 
char *writeout (char *buffer, char *file, int *ptr);

/* server.c */
int check_seesock();
int file_check (char *file);
int getMsg (int fd, char *buffer, int len);
int loclcon (char *sock);
int loclsckt (char *file); 
int send_remote (char *term);
void *serverLoop (void *fd);
gboolean takecall(GIOChannel *gio, GIOCondition ignored);
void tglServer (GtkWidget *ignored, int); 

/* common globals */
extern int Debug;
extern forconfig *Config;
extern seefile *Seefile;
extern forblink Sff;
extern GtkWidget *ServTgl, *ServOff, *ServOn, *Fent;
extern char *EXDline;
extern GIOChannel *Gio;
